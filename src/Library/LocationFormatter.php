<?php

declare(strict_types=1);

namespace App\Library;

class LocationFormatter
{
    public function format(array $locations): array
    {
        foreach ($locations as $key => $value) {
            $locations[$key] = [
                'time' => $this->formatTime(substr($value, 0, 6)),
                'latitude' => $this->formatLatitude(substr($value, 6, 8)),
                'longitude' => $this->formatLongitude(substr($value, 14, 9))
            ];
        }

        return $locations;
    }

    private function formatTime(string $time): string
    {
        return sprintf(
            '%s:%s:%s',
            substr($time, 0, 2),
            substr($time, 2, 2),
            substr($time, 4, 2)
        );
    }

    private function formatLatitude(string $latitude): string
    {
        return sprintf(
            '%s°%s.%s\' %s',
            ltrim(substr($latitude, 0, 2), '0'),
            ltrim(substr($latitude, 2, 2),'0'),
            substr($latitude, 4, 3),
            substr($latitude, -1, 1)
        );
    }

    private function formatLongitude(string $longitude): string
    {
        return sprintf(
            '%s°%s.%s\' %s',
            ltrim(substr($longitude, 0, 3), '0'),
            ltrim(substr($longitude, 3, 2), '0'),
            substr($longitude, 5, 3),
            substr($longitude, -1, 1)
        );
    }
}
