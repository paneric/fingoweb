<?php

declare(strict_types=1);

namespace App\Library;

class LocationExtractor
{
    private $downloadManager;

    private $baseUrl;

    public function __construct(
        DownloadManager $downloadManager,
        string $baseUrl
    ) {
        $this->downloadManager = $downloadManager;

        $this->baseUrl = $baseUrl;
    }

    public function extract(string $date, string $fileName): array
    {
        $igcData = $this->downloadManager->execute(
            $this->prepareUrl($date, $fileName)
        );

        if ($igcData === null) {
            return $this->getErrorMessage();
        }

        return $this->filterIgcData($igcData);
    }

    private function prepareUrl(string $date, string $fileName): string
    {
        return sprintf(
            '%s/%s/%s',
            $this->baseUrl,
            $date,
            $fileName
        );
    }

    private function getErrorMessage(): array
    {
        return [
            'error' => $this->downloadManager->getErrorMessage(),
        ];
    }

    private function filterIgcData($igcData): array
    {
        foreach ($igcData as $key => $value) {
            if (strpos($value, 'B') === 0) {
                $igcData[$key] = ltrim($value, 'B');
            } else {
                unset($igcData[$key]);
            }
        }

        return array_values($igcData);
    }
}
