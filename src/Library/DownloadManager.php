<?php

declare(strict_types=1);

namespace App\Library;

class DownloadManager
{
    private $options;

    private $content;
    private $errorMessage;
    private $header;

    public function __construct()
    {
        $this->options = [
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/octet-stream'
            ],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER => false,
        ];
    }

    public function execute(string $url): ?array
    {
        $curlHandle = curl_init($url);

        curl_setopt_array($curlHandle, $this->options);

        $this->content = curl_exec($curlHandle);
        $errorNumber = curl_errno($curlHandle);
        $this->errorMessage = curl_error($curlHandle);
        $this->header = curl_getinfo($curlHandle);

        curl_close($curlHandle);

        if ($errorNumber === 0) {
            return explode("\n", $this->content);
        }

        return null;
    }

    /**
     * @return string|bool
     */
    public function getContent()
    {
        return $this->content;
    }

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    /**
     * @return string|array
     */
    public function getHeader()
    {
        return $this->header;
    }
}
