<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\LocationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class LocationController extends AbstractController
{
    private $locationService;

    public function __construct(LocationService $locationService)
    {
        $this->locationService = $locationService;
    }

    public function showAll(string $date, string $fileName): Response
    {
        return $this->render(
            'location/show_all.html.twig',
            $this->locationService->getAll($date, $fileName)
        );
    }

    public function showRange(string $date, string $fileName): Response
    {
        return $this->render(
            'location/show_range.html.twig',
            $this->locationService->getRange($date, $fileName)
        );
    }

    public function show(string $date, string $fileName, string $key): Response
    {
        return $this->render(
            'location/show.html.twig',
            $this->locationService->get($date, $fileName, (int) $key)
        );
    }
}
