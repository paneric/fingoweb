<?php

declare(strict_types=1);

namespace App\Service;

use App\Library\LocationExtractor;
use App\Library\LocationFormatter;

class LocationService
{
    private $locationExtractor;
    private $locationFormatter;

    private $limit;

    public function __construct(
        LocationExtractor $locationExtractor,
        LocationFormatter $locationFormatter,
        int $limit
    ) {
        $this->locationExtractor = $locationExtractor;
        $this->locationFormatter = $locationFormatter;

        $this->limit = $limit;
    }

    public function getAll(string $date, string $fileName): array
    {
        $locations = $this->locationExtractor->extract($date, $fileName);

        if (isset($locations['error'])) {
            return $locations;
        }

        return [
            'date' => $date,
            'locations' => $this->locationFormatter->format($locations),
        ];
    }

    public function getRange(string $date, string $fileName): array
    {
        $locations = $this->locationExtractor->extract($date, $fileName);

        if (isset($locations['error'])) {
            return $locations;
        }

        $locations = array_slice(
            $locations,
            0,
            $this->limit
        );

        return [
            'date' => $date,
            'locations' => $this->locationFormatter->format($locations),
        ];
    }

    public function get(string $date, string $fileName, int $key): array
    {
        $locations = $this->locationExtractor->extract($date, $fileName);

        if (isset($locations['error'])) {
            return $locations;
        }

        if ($key === -1) {
            return [
                'key' => $key,
                'date' => $date,
                'locations' => $this->locationFormatter->format([
                    end($locations)
                ]),
            ];
        }

        return [
            'key' => $key,
            'date' => $date,
            'locations' => $this->locationFormatter->format([
                $locations[$key - 1]
            ]),
        ];
    }
}
