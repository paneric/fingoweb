# FingoWeb

Junior PHP Developer test challenge

## Required

* PHP >= 7.2.5

## Tested environment

* **Linux Ubuntu 18.04.5**
* **PHP 7.3.27**  
* **Symfony 4.23.5**

## Folder structure

* **cloud-services-api**
    * **config**
        * ...
        * routes.yaml
        * services.yaml
    * **src**
        * **Controller**
            * FlightController.php 
            * LocationController.php 
        * **Library**
            * DownloadManager.php 
            * LocationExtractor.php 
            * LocationFormatter.php 
        * **Service**
            * LocationService.php 
    * **templates**
        * **flight**
            * index.html.twig
        * **location**
            * show.html.twig    
            * show_all.html.twig
            * show_range.html.twig
        * base.html.twig
    * composer.json
    * README.md  

## Installation

Within chosen location:

```sh
$ git clone https://paneric@bitbucket.org/paneric/fingoweb.git
$ cd fingoweb
$ composer install
$ symfony server:start

 [OK] Web server listening                                                                                              
      The Web server is using PHP CLI 7.3.27                                                                            
      http://127.0.0.1:8001 
```
## Features

### Start Page - Flights

```
http://127.0.0.1:8001 
```
> Port number follows Symfony server initialization message. 

![image info](./public/img/fingoweb-flights.png)

There are three exemplary flights considered.
Each flight record consists of pilot name and surname followed by links to detailed locations' information:

* **FLIGHT LOCATIONS (START POINT)**
* **FLIGHT LOCATIONS (END POINT)**
* **FLIGHT LOCATIONS (COURSE)** - complete list of flight locations
* **FLIGHT LOCATIONS (FIRST 10 RECORDS )** - flight locations range of first 10 records

![image info](./public/img/fingoweb-location-start-point.png)
![image info](./public/img/fingoweb-location-end-point.png)
![image info](./public/img/fingoweb-location-point-list.png)
![image info](./public/img/fingoweb-location-first-10-points.png)
